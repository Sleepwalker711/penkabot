import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    java
    kotlin("jvm") version "1.3.70"
    kotlin("plugin.serialization") version "1.3.70"
    id("com.github.johnrengelman.shadow") version "5.2.0"
}
group = "ru.sleep7"
version = "1.0-SNAPSHOT"

dependencies {
    // Kotlin
    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib"))
    implementation(kotlin("serialization"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")

    // Ktor
    implementation("io.ktor:ktor-client-websocket:1.1.5")
    implementation("io.ktor:ktor-client-cio:1.1.5")
    implementation("io.ktor:ktor-client-js:1.1.5")
    implementation("io.ktor:ktor-client-okhttp:1.1.4")
    implementation("io.ktor:ktor-server-netty:1.1.5")
    implementation("io.ktor:ktor-server-core:1.1.5")
    implementation("io.ktor:ktor-gson:1.1.5")

    // Discord
    implementation("com.sedmelluq:lavaplayer:1.3.53")
    implementation("net.dv8tion:JDA:4.2.0_212")

    // Polly
    implementation("com.amazonaws:aws-java-sdk-polly:1.11.552")

    // Utils
    implementation("io.github.microutils:kotlin-logging:1.7.6")
    implementation("org.apache.commons:commons-lang3:3.9")
    implementation("org.reflections:reflections:0.9.11")
    implementation("org.slf4j:slf4j-simple:2.0.0-alpha0")
    implementation("org.slf4j:slf4j-api:2.0.0-alpha0")
    implementation("com.charleskorn.kaml:kaml:0.17.0")

    // Database
    implementation("org.jetbrains.exposed:exposed:0.15.1")
    implementation("org.postgresql:postgresql:42.2.6")

    implementation("com.beust:klaxon:5.0.1")
}

repositories {
    mavenCentral()
    jcenter()
    maven("http://jcenter.bintray.com")
    maven("http://repo.bastian-oppermann.de")
    maven("https://dl.bintray.com/kotlin/ktor")
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}