package ru.sleep7.utils

class Sometimes(val timesPerSecond: Double, executeAtFirstTime: Boolean) {

    private val stepMs = 1000 / timesPerSecond
    private var lastCall = if (!executeAtFirstTime) System.currentTimeMillis() else 0

    public fun perform(block: () -> Unit): Boolean {
        if (System.currentTimeMillis() - lastCall > stepMs) {
            block()
            lastCall = System.currentTimeMillis()
            return true;
        }
        return false;
    }
}