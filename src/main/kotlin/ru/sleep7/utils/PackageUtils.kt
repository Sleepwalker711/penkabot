package ru.sleep7.utils

import mu.KotlinLogging
import kotlin.reflect.KClass
import org.reflections.Reflections

inline fun <reified T> listClasses(packageName: String) : ArrayList<KClass<*>> {
    val reflections = Reflections(packageName)
    val clss = reflections.getSubTypesOf(T::class.java)

    val result = ArrayList<KClass<*>>()

    clss.forEach {
        val kClass: Class<*> = it

        if (kClass.kotlin.qualifiedName?.startsWith(packageName) == true) {
            result.add(kClass.kotlin)
        }
    }

    return result
}