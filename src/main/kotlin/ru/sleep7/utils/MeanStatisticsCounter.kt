package ru.sleep7.utils

import org.apache.commons.collections4.queue.CircularFifoQueue

class MeanStatisticsCounter(size: Int) {
    private val buffer = CircularFifoQueue<Double>(size)

    fun push(value: Double) {
        buffer.add(value)
    }

    fun getValue(): Double {
        return buffer.sum() / buffer.size
    }
}