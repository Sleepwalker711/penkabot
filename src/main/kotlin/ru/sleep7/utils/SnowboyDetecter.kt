package ru.sleep7.utils

import ai.kitt.snowboy.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import ru.sleep7.configuration.SnowboyConfig
import java.nio.ByteBuffer
import java.nio.ByteOrder

import javax.sound.sampled.AudioFormat

private val logger = KotlinLogging.logger{}

class SnowboyDetecter(config: SnowboyConfig) {
    companion object {
        val INPUT_FORMAT = AudioFormat(16000.0f, 16, 1, true, false)
    }

    private var detector: SnowboyDetect
    init {
        System.loadLibrary("snowboy-detect-java");
        detector = SnowboyDetect(config.resource_path, config.model_path)
        logger.info{ "Snowboy detection enabled "}
        detector.SetSensitivity(config.sensitivity.toString())
        detector.SetAudioGain(config.audio_gain)
        detector.ApplyFrontend(false)
    }

    fun detect(data: ByteArray): Boolean {

        val arr = ShortArray(data.size / 2)
        ByteBuffer.wrap(data).order(
                ByteOrder.LITTLE_ENDIAN
        ).asShortBuffer().get(arr)

        return detector.RunDetection(arr, arr.size) > 0;
    }
}