package ru.sleep7.utils

private val parsers = hashMapOf(
        "^(\\d+)$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() },
        "^(\\d+)s$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() },
        "^(\\d+)m$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() * 60 },
        "^(\\d+)h$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() * 60 * 60 },
        "^(\\d+):(\\d+)\$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() * 60 + r.groupValues[2].toInt() },
        "^(\\d+):(\\d+):(\\d+)$".toRegex() to
                { r: MatchResult ->
                    r.groupValues[1].toInt() * 60 * 60 + r.groupValues[2].toInt() * 60 + r.groupValues[3].toInt() }
)

fun parseToSeconds(time: String): Int {
    parsers.forEach{
        val match = it.key.matchEntire(time)

        if (match != null) {
            return it.value(match)
        }
    }

    return -1
}

fun secondToDateString(seconds: Int): String {
    // HH:MM:SS
    val builder = StringBuilder()
    builder.append((seconds / (60 * 60)).toString().padStart(2, '0'))
    builder.append(":")
    builder.append((seconds % (60 * 60) / 60).toString().padStart(2, '0'))
    builder.append(":")
    builder.append((seconds % 60).toString().padStart(2, '0'))
    return builder.toString()
}