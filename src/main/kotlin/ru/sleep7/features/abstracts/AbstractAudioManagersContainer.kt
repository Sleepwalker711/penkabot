package ru.sleep7.features.abstracts

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import net.dv8tion.jda.api.entities.Guild
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.penkabot.Context
import ru.sleep7.player.GuildAudioManager
import ru.sleep7.player.MixingAudioSendHandler

abstract class AbstractAudioManagersContainer(context: Context) : AbstractFeature(context) {
    private val audioManagers: MutableMap<String, GuildAudioManager> = mutableMapOf()

    @Synchronized
    fun getGuildAudioManager(guild: Guild): GuildAudioManager {
        var audioManager = audioManagers[guild.id]

        if (audioManager == null) {
            audioManager = GuildAudioManager(penkaContext.playerManager, object : AudioEventAdapter() {
                override fun onTrackEnd(player: AudioPlayer?, track: AudioTrack?, endReason: AudioTrackEndReason?) {
                    penkaContext.featuresContainer.findFeature<MusicPlayer>().updateStatus()
                }

                override fun onPlayerResume(player: AudioPlayer?) {
                    penkaContext.featuresContainer.findFeature<MusicPlayer>().updateStatus()
                }

                override fun onTrackStart(player: AudioPlayer?, track: AudioTrack?) {
                    penkaContext.featuresContainer.findFeature<MusicPlayer>().updateStatus()
                }

                override fun onPlayerPause(player: AudioPlayer?) {
                    penkaContext.featuresContainer.findFeature<MusicPlayer>().updateStatus()
                }
            })
            audioManagers[guild.id] = audioManager
        }

        if (guild.audioManager.sendingHandler == null) {
            guild.audioManager.sendingHandler = MixingAudioSendHandler()
        }

        val mixer = guild.audioManager.sendingHandler as MixingAudioSendHandler
        mixer.audioSendHandlers.add(audioManager.sendHandler)

        return audioManager
    }
}