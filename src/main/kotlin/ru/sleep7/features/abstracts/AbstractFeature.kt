package ru.sleep7.features.abstracts

import ru.sleep7.penkabot.Context

abstract class AbstractFeature(val penkaContext: Context) {
    open fun initialize() {

    }
}