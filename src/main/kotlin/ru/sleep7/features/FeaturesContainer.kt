package ru.sleep7.features

import mu.KotlinLogging
import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context
import ru.sleep7.utils.listClasses
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

private val logger = KotlinLogging.logger {}
class FeaturesContainer(context: Context) {

    private val featuresClasses: ArrayList<KClass<*>> = listClasses<AbstractFeature>(
            "ru.sleep7.features.implementations")

    private var featuresContainer: HashMap<String, AbstractFeature> = hashMapOf()

    init {
        logger.info{ "Building features" }

        val args = HashMap<String, Any?>().apply {
            put("context", context)
        }

        featuresClasses.forEach { featureClass ->
            logger.info{ "Creating feature \"${featureClass.simpleName}\"" }
            val constructor = featureClass.primaryConstructor
            val arguments = HashMap<KParameter, Any?>().apply {
                constructor?.parameters?.forEach { parameter ->
                    if (parameter.name in args) {
                        put(parameter, args[parameter.name])
                    }
                }
            }

            featuresContainer[featureClass.simpleName!!] = constructor?.callBy(arguments) as AbstractFeature
        }
    }

    fun findFeature(name: String): AbstractFeature? {
        return featuresContainer[name]
    }

    inline fun <reified T> findFeature(): T {
        return findFeature(T::class.simpleName!!) as T
    }

    fun performInitialization() {
        featuresContainer.forEach { (_, feature) ->
            feature.initialize()
        }
    }
}