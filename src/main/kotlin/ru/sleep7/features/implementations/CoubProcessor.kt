package ru.sleep7.features.implementations

import net.dv8tion.jda.api.entities.Message
import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context

class CoubProcessor(context: Context) : AbstractFeature(context) {

    private val regex = "https?://coub.com/\\w*/\\w*".toRegex()

    fun doesMessageContainsCoub(message: Message): Boolean {
        return regex.containsMatchIn(message.contentRaw)
    }

    fun getCoubLink(message: Message): String {
        return regex.find(message.contentRaw, 0)!!.groups[0]!!.value
    }
}