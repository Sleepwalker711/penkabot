package ru.sleep7.features.implementations

import mu.KotlinLogging
import net.dv8tion.jda.api.MessageBuilder
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context

private val logger = KotlinLogging.logger{}

class StatusManager(context: Context) : AbstractFeature(context) {

    class Status(private val context: Context, private val messageId: String) {
        fun setText(arg: MessageEmbed) {
            context.penkaChannel.editMessageById(
                    messageId,
                    arg
            ).queue()
        }

        fun setText(arg: Message) {
            context.penkaChannel.editMessageById(
                    messageId,
                    arg
            ).queue()
        }

        fun setText(arg: CharSequence) {
            context.penkaChannel.editMessageById(
                    messageId,
                    arg
            ).queue()
        }
    }

    private val statusesMap: HashMap<String, Status> = hashMapOf()

    init {
        context.config.statuses.status_messages.forEach{ (name, message_id) ->
            statusesMap[name] = Status(context, message_id)
        }
    }

    fun getStatusFor(name: String): Status {
        if (!statusesMap.containsKey(name)) {
            logger.error { "No status message in config for '$name'" }
        }

        return statusesMap[name]!!
    }

    inline fun <reified T> getStatusFor(): Status {
        return getStatusFor(T::class.simpleName!!)
    }

    fun resetPinned() {
        // clearing current pinned messages
        val pinned = penkaContext.penkaChannel.retrievePinnedMessages().complete()
        pinned.forEach{message ->
            penkaContext.penkaChannel.unpinMessageById(message.id).complete()
        }

        penkaContext.config.statuses.status_messages.forEach { (_, message_id) ->
            penkaContext.penkaChannel.pinMessageById(message_id).complete()
        }
    }

    override fun initialize() {
        resetPinned()
    }
}