package ru.sleep7.features.implementations

import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context

class FollowProcessor(val context: Context) : AbstractFeature(context) {
    public fun joinToOwner() {
        penkaContext.jda.voiceChannels.firstOrNull { it.members.map { it.user }.contains(penkaContext.targetUser) }?.let {
            penkaContext.penkaGuild.audioManager.openAudioConnection(it)
            context.currentVoiceChannel = it
        }
    }
}