package ru.sleep7.features.implementations

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.MessageBuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.managers.AudioManager
import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context
import ru.sleep7.player.GuildAudioManager
import ru.sleep7.utils.secondToDateString
import java.util.concurrent.LinkedBlockingQueue

class MusicPlayer(context: Context) : AbstractFeature(context) {

    private var status: StatusManager.Status? = null

    override fun initialize() {
        status = penkaContext.featuresContainer.findFeature<StatusManager>()
                .getStatusFor<MusicPlayer>();

        // clearing text and adding embed object
        updateStatus()
    }

    fun updateStatus() {
        status?.setText(buildEmbedInfo(penkaContext.penkaGuild))
    }

    fun loadAndPlay(guild: Guild, trackUrl: String) {
        val audioManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        audioManager.playerManager.loadItemOrdered(audioManager, trackUrl, object : AudioLoadResultHandler {
            override fun trackLoaded(track: AudioTrack) {
                penkaContext.penkaChannel.sendMessage("Добавляю в очередь \"${track.info.title}\"").queue()
                play(guild, audioManager, track)

                updateStatus()
            }

            override fun playlistLoaded(playlist: AudioPlaylist) {
                var firstTrack: AudioTrack? = playlist.selectedTrack

                if (firstTrack == null) {
                    firstTrack = playlist.tracks[0]
                }

                penkaContext.penkaChannel.sendMessage("Добавляю в очередь \"${firstTrack!!.info.title}\" (первый трек из плейлиста \"${playlist.name}\")").queue()

                play(guild, audioManager, firstTrack)
            }

            override fun noMatches() {
                penkaContext.penkaChannel.sendMessage("По запросу \"$trackUrl\" ничего не найдено").queue()
            }

            override fun loadFailed(exception: FriendlyException) {
                penkaContext.penkaChannel.sendMessage("Не получается проиграть: \"${exception.message}\"").queue()
            }
        })
    }

    fun position(guild: Guild): Int {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        if (musicManager.player.playingTrack == null) {
            return -1
        }

        return (musicManager.player.playingTrack.position / 1000L).toInt()
    }

    fun seek(guild: Guild, seconds: Int): Boolean {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        if (!musicManager.player.playingTrack.isSeekable) {
            return false
        }

        musicManager.player.playingTrack.position = seconds * 1000L // seconds to ms

        return true
    }

    fun hasActiveTrack(guild: Guild): Boolean {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        return musicManager.player.playingTrack != null
    }

    fun skip(guild: Guild) {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        musicManager.scheduler.nextTrack()
    }

    fun unpause(guild: Guild) {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)
        musicManager.player.isPaused = false
    }

    fun pause(guild: Guild) {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)
        musicManager.player.isPaused = true
    }

    fun pauseUnpause(guild: Guild): Boolean {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)
        musicManager.player.isPaused = !musicManager.player.isPaused
        return musicManager.player.isPaused
    }

    fun play(guild: Guild, audioManager: GuildAudioManager, track: AudioTrack) {
        connectToFirstVoiceChannel(guild.audioManager)
        audioManager.scheduler.queue(track)
    }

    fun connectToFirstVoiceChannel(audioManager: AudioManager) {
        if (!audioManager.isConnected && !audioManager.isAttemptingToConnect) {
            for (voiceChannel in audioManager.guild.voiceChannels) {
                audioManager.openAudioConnection(voiceChannel)
                break
            }
        }
    }

    fun currentPlayingTrack(guild: Guild): AudioTrack? {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)

        return musicManager.player.playingTrack;
    }

    fun trackList(guild: Guild): LinkedBlockingQueue<AudioTrack> {
        val musicManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(guild)
        return musicManager.scheduler.getQueue()
    }

    fun buildEmbedInfo(guild: Guild): Message {
        val trackList = trackList(guild)
        val currentTrack = currentPlayingTrack(guild)
        val currentTrackString = if (currentTrack != null) {
            "${fetchTrackInfoTitle(currentTrack)}(${secondToDateString(currentTrack.position.toInt() / 1000)}) ${currentTrack.info.uri}"
        } else {
            "Ничего не воспроизводится"
        }

        val embedBuilder = EmbedBuilder()
                .setTitle("Текущий плейлист")
                .setDescription(":notes: $currentTrackString")

        trackList.forEach {
            embedBuilder.addField(
                    fetchTrackInfoTitle(it),
                    it.info.uri,
                    false
            )
        }

        return MessageBuilder().setEmbed(embedBuilder.build()).setContent(" ").build()
    }

    fun fetchTrackInfoTitle(it: AudioTrack): String {
        return "${it.info.author} - \"${it.info.title}\": ${secondToDateString(it.duration.toInt() / 1000)}"
    }

}