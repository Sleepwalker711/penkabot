package ru.sleep7.features.implementations

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.polly.AmazonPollyClient
import com.amazonaws.services.polly.model.OutputFormat
import com.amazonaws.services.polly.model.SynthesizeSpeechRequest
import com.amazonaws.services.polly.model.VoiceId
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import mu.KotlinLogging
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.managers.AudioManager
import org.apache.commons.io.FileUtils
import ru.sleep7.features.abstracts.AbstractFeature
import ru.sleep7.penkabot.Context
import java.io.File
import java.io.InputStream
import java.security.MessageDigest

private val logger = KotlinLogging.logger{}

class PollyTextToSpeech(context: Context) : AbstractFeature(context) {

    private val polly = AmazonPollyClient.builder()
            .withCredentials(
                    AWSStaticCredentialsProvider(
                            BasicAWSCredentials(
                                    context.config.polly.access_key,
                                    context.config.polly.secret_key
                            )
                    )
            )
            .withRegion(Regions.EU_WEST_2)
            .build()

    private val messageDigest = MessageDigest.getInstance("SHA-256")

    fun say(text: String, guild: Guild, voice: VoiceId = VoiceId.Maxim) {
        logger.info{"Saying '$text'"}
        val pathToSpeech = generatePhrase(text, voice)
        loadAndPlay(guild, pathToSpeech)
    }

    private fun loadAndPlay(guild: Guild, pathToSpeech: String) {
        val audioManager = penkaContext.featuresContainer.findFeature<TTSAudioManagers>()
                .getGuildAudioManager(guild)
        audioManager.playerManager.loadItemOrdered(audioManager, pathToSpeech, object : AudioLoadResultHandler {
            override fun trackLoaded(track: AudioTrack) {
                connectToFirstVoiceChannel(guild.audioManager)
                audioManager.scheduler.queue(track)
            }

            override fun playlistLoaded(playlist: AudioPlaylist) {
                var firstTrack: AudioTrack? = playlist.selectedTrack

                if (firstTrack == null) {
                    firstTrack = playlist.tracks[0]
                }

                connectToFirstVoiceChannel(guild.audioManager)
                audioManager.scheduler.queue(firstTrack!!)
            }

            override fun noMatches() {
                logger.error{"No such file: $pathToSpeech"}
            }

            override fun loadFailed(exception: FriendlyException) {
                logger.error{"Loading failed: ${exception.fillInStackTrace()}"}
            }
        })
    }

    private fun generatePhrase(text: String, voice: VoiceId): String {
        val digest = messageDigest.digest(
                (text + voice).toByteArray()
        ).fold("", { str, it -> str + "%02x".format(it) })

        val filepath = "${penkaContext.config.polly.cache_path}/$digest.mp3"

        // Using cached file if already exists
        if (File(filepath).exists()) {
            return filepath
        }

        // Create new one otherwise
        FileUtils.copyInputStreamToFile(
                polly.synthesizeSpeech(SynthesizeSpeechRequest()
                        .withText(text)
                        .withVoiceId(voice)
                        .withOutputFormat(OutputFormat.Mp3))
                        .audioStream,
                File(filepath)
        )

        return filepath
    }

    private fun connectToFirstVoiceChannel(audioManager: AudioManager) {
        if (!audioManager.isConnected && !audioManager.isAttemptingToConnect) {
            for (voiceChannel in audioManager.guild.voiceChannels) {
                audioManager.openAudioConnection(voiceChannel)
                break
            }
        }
    }
}