package ru.sleep7.features.implementations

import ru.sleep7.features.abstracts.AbstractAudioManagersContainer
import ru.sleep7.penkabot.Context

class MusicAudioManagers(context: Context) : AbstractAudioManagersContainer(context)