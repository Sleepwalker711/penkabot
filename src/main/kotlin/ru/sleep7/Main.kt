package ru.sleep7

import ru.sleep7.configuration.loadConfig
import ru.sleep7.db.DatabaseInitializer
import ru.sleep7.penkabot.PenkaBot

fun main(args: Array<String>) {

    if (args.size != 1) {
        println("Please provide path to configs as first argument")
        return
    }

    val config = loadConfig(args[0])

    DatabaseInitializer(config.database)
    PenkaBot(config).start()
}
