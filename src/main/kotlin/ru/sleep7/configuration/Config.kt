package ru.sleep7.configuration

import com.charleskorn.kaml.Yaml
import java.io.File

@kotlinx.serialization.Serializable
data class BasicConfig(
        val commands_prefix: String,
        val web_service_port: Int,
        val default_music_volume: Int
)

@kotlinx.serialization.Serializable
data class DatabaseConfig(
        val driver: String,
        val connection_string: String,
        val user: String,
        val password: String
)

@kotlinx.serialization.Serializable
data class VoiceCommandsConfig(
        val enabled: Boolean
)

@kotlinx.serialization.Serializable
data class StatusesConfig(
        val status_messages: Map<String, String>
)

@kotlinx.serialization.Serializable
data class NotificationMessages(
        val on_join: List<String>,
        val on_leave: List<String>,
        val on_move_out: List<String>,
        val on_move_in: List<String>,
        val on_mute: List<String>,
        val on_unmute: List<String>,
        val on_deaf: List<String>,
        val on_hear: List<String>
)

@kotlinx.serialization.Serializable
data class MemberStatusNotification(
        val messages: NotificationMessages
)

@kotlinx.serialization.Serializable
data class ListenersConfig(
        val voice_commands: VoiceCommandsConfig,
        val enabled_listeners: List<String>
)

@kotlinx.serialization.Serializable
data class DiscordIds(
        val main_guild: String,
        val main_text_channel: String,
        val default_main_user: String,
        val default_voice_channel: String,
        val coub_text_channel: String,
        val tts_text_channel: String
)

@kotlinx.serialization.Serializable
data class DiscordConfig(
        val api_token: String,
        val ids: DiscordIds
)

@kotlinx.serialization.Serializable
data class PollyConfig(
        val access_key: String,
        val secret_key: String,
        val cache_path: String
)

@kotlinx.serialization.Serializable
data class SnowboyConfig(
        val enabled: Boolean,
        val resource_path: String,
        val model_path: String,
        val sensitivity: Float,
        val audio_gain: Float
)

@kotlinx.serialization.Serializable
data class DeepSpeechConfig(
        val enabled: Boolean,
        val host: String,
        val port: Int,
        val path: String,
        val check_delay_ms: Long,
        val reconnect_time_ms: Long,
        val required_user_frames_to_send: Int,
        val minimum_frames_to_send: Int
)

@kotlinx.serialization.Serializable
data class YandexConfig(
        val token: String,
        val folder_id: String
)

@kotlinx.serialization.Serializable
class Config (
        val basic: BasicConfig,
        val database: DatabaseConfig,
        val member_status_notification: MemberStatusNotification,
        val statuses: StatusesConfig,
        val listeners: ListenersConfig,
        val discord: DiscordConfig,
        val polly: PollyConfig,
        val snowboy: SnowboyConfig,
        val deepspeech: DeepSpeechConfig,
        val yandex: YandexConfig
)

fun loadConfig(path: String): Config {
    return Yaml.default.parse(Config.serializer(), File(path).readText())
}