package ru.sleep7.player

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import mu.KotlinLogging
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

private val logger = KotlinLogging.logger{}

class TrackScheduler(private val player: AudioPlayer) : AudioEventAdapter() {
    private val queue = LinkedBlockingQueue<AudioTrack>()

    fun queue(track: AudioTrack) {
        logger.info("Queued \"${track.info.title}\"")
        if (!player.startTrack(track, true)) {
            queue.offer(track)
        }
    }

    fun nextTrack() {
        player.startTrack(queue.poll(), false)
    }

    override fun onTrackEnd(player: AudioPlayer?, track: AudioTrack?, endReason: AudioTrackEndReason?) {
        logger.info("Track \"${track?.info?.title}\" was ended with reason \"$endReason\"")
        if (endReason!!.mayStartNext) {
            nextTrack()
        }
    }

    fun getQueue(): LinkedBlockingQueue<AudioTrack> {
        return queue
    }
}