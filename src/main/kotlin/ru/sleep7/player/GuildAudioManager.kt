package ru.sleep7.player

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter

class GuildAudioManager(val playerManager: AudioPlayerManager, outdoor: AudioEventAdapter? = null) {
    val player: AudioPlayer = playerManager.createPlayer()
    val scheduler: TrackScheduler = TrackScheduler(player)
    val sendHandler: AudioPlayerSendHandler = AudioPlayerSendHandler(player)

    init {
        player.addListener(scheduler)

        if (outdoor != null) {
            player.addListener(outdoor)
        }
    }
}
