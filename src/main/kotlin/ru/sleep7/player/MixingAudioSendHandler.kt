package ru.sleep7.player

import mu.KotlinLogging
import net.dv8tion.jda.api.audio.AudioReceiveHandler.OUTPUT_FORMAT
import net.dv8tion.jda.api.audio.AudioSendHandler
import org.apache.commons.collections4.set.ListOrderedSet
import java.lang.RuntimeException
import java.nio.ByteBuffer
import java.nio.ByteOrder

private val logger = KotlinLogging.logger{}

class MixingAudioSendHandler : AudioSendHandler {

    val audioSendHandlers = ListOrderedSet<AudioSendHandler>()

    private val cachedResultBuffer = ByteBuffer.allocate(calcBufferSize())
    private val cachedBufferOfShorts = ShortArray(calcBufferSize() / 2) { 0 }

    override fun provide20MsAudio(): ByteBuffer? {
        val result = ShortArray(calcBufferSize() / 2) { 0 }

        audioSendHandlers.forEach {
            val buffer = fetchPCMFromHandler(it)
            if (buffer != null) {
                buffer.order(
                        ByteOrder.BIG_ENDIAN
                ).asShortBuffer().get(cachedBufferOfShorts)

                if (cachedBufferOfShorts.size != result.size) {
                    throw RuntimeException("Buffer mismatch size ${buffer.capacity()} != ${result.size}")
                }

                for (i in 0 until cachedBufferOfShorts.size) {
                    result[i] = (result[i] + cachedBufferOfShorts[i]).toShort()
                }
            }
        }

        cachedResultBuffer.asShortBuffer().put(result)
        return cachedResultBuffer
    }

    override fun canProvide(): Boolean {
        audioSendHandlers.forEach {
            if (it.canProvide()) {
                return true
            }
        }
        return false
    }

    override fun isOpus(): Boolean {
        return false
    }

    private fun fetchPCMFromHandler(handler: AudioSendHandler): ByteBuffer? {
        val data = handler.provide20MsAudio() ?: return null

        if (handler.isOpus) {
            throw RuntimeException("Opus output is not supported for mixing")
        }

        return data
    }

    private fun calcBufferSize(): Int {
        val format = OUTPUT_FORMAT

        return (((format.sampleRate / 1000) * 20) * (format.sampleSizeInBits / 8) * format.channels).toInt()
    }
}