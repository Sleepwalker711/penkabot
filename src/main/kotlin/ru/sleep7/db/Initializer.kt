package ru.sleep7.db

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import ru.sleep7.configuration.DatabaseConfig
import ru.sleep7.db.dao.Coubs

fun DatabaseInitializer(config: DatabaseConfig) {
    Database.connect(
            url = config.connection_string,
            driver = config.driver,
            user = config.user,
            password = config.password
    )

    transaction {
        SchemaUtils.createMissingTablesAndColumns(Coubs)
        commit()
    }
}