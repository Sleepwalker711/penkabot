package ru.sleep7.db.dao

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Coubs : IntIdTable() {
    val title = varchar("title", 200)
    val url = varchar("url", 2083)
    val postedBy = varchar("postedBy", 100)
    val date = datetime("date")
}

class Coub (id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Coub>(Coubs)

    var title by Coubs.title
    var url by Coubs.url
    var postedBy by Coubs.postedBy
    var date by Coubs.date
}