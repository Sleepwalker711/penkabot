package ru.sleep7.db


import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import ru.sleep7.db.dao.Coub
import ru.sleep7.db.dao.Coubs

fun saveCoub(title: String, url: String, postedBy: String, date: DateTime) {
    transaction {
        Coub.new {
            this.title = title
            this.url = url
            this.postedBy = postedBy
            this.date = date
        }
        commit()
    }
}

fun getCoubById(id: Int): Coub {
    return transaction {
        return@transaction Coub[id]
    }
}

fun getCoubByUrl(url: String): Coub? {
    return transaction {
        return@transaction Coub.find{Coubs.url eq url}.firstOrNull()
    }
}