package ru.sleep7.listeners.simpleVoiceListeners

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mu.KotlinLogging
import net.dv8tion.jda.api.audio.AudioReceiveHandler.OUTPUT_FORMAT
import net.dv8tion.jda.api.audio.UserAudio
import ru.sleep7.clients.DeepSpeechClient
import ru.sleep7.clients.YandexSpeechAPI
import ru.sleep7.features.implementations.PollyTextToSpeech
import ru.sleep7.listeners.abstracts.AbstractSimpleVoiceListener
import ru.sleep7.penkabot.Context
import ru.sleep7.utils.MeanStatisticsCounter
import ru.sleep7.utils.SnowboyDetecter
import ru.sleep7.utils.Sometimes
import java.io.ByteArrayInputStream
import java.lang.RuntimeException
import javax.sound.sampled.AudioInputStream
import javax.sound.sampled.AudioSystem
import kotlin.system.measureTimeMillis

private val logger = KotlinLogging.logger{}

class VoiceCommandListener(val context: Context) : AbstractSimpleVoiceListener {

    private val packetDuration: Int = 20 // ms
    private val maxQueueSize: Int = 30 * 1000 / packetDuration // sec * ms/sec
    private val proceedStepPackets: Int = 500 / packetDuration

    private var deepspeechClient: DeepSpeechClient? = null
    private var snowboyDetecter: SnowboyDetecter? = null

    private var yandexSpeechClient = YandexSpeechAPI(context.config.yandex)

    private val encodingSpeedCounter = MeanStatisticsCounter(1000 / 20)
    private val sometimes = Sometimes(0.5, false)

    private var commandProviderID = ""
    private var commandListeningEnabled = false
    private var commandAudioData = ArrayList<Byte>()
    private var commandListeningStartTime = System.currentTimeMillis()

    init {

        if (context.config.deepspeech.enabled) {
            if(!AudioSystem.isConversionSupported(DeepSpeechClient.INPUT_FORMAT, OUTPUT_FORMAT)) {
                throw RuntimeException("Can't convert audio for deepspeech...")
            }

            deepspeechClient = DeepSpeechClient(context.config.deepspeech)
        }

        if (context.config.snowboy.enabled) {
            if(!AudioSystem.isConversionSupported(SnowboyDetecter.INPUT_FORMAT, OUTPUT_FORMAT)) {
                throw RuntimeException("Can't convert audio for snowboy...")
            }

            snowboyDetecter = SnowboyDetecter(context.config.snowboy)
        }
    }

    override fun isEnabled(): Boolean {
        return context.config.listeners.voice_commands.enabled
    }

    override fun canReceiveUser(): Boolean {
        return true
    }

    override fun handleUserAudio(userAudio: UserAudio) {
        if (userAudio.user.id != context.targetUser.id) {
            return
        }

        val data = userAudio.getAudioData(1.0)

        val bitsInByte = 8
        val processingTime = measureTimeMillis {
            val sourceStream = AudioInputStream(ByteArrayInputStream(data), OUTPUT_FORMAT, (data.size / (OUTPUT_FORMAT.sampleSizeInBits / bitsInByte)).toLong());
            val converted = AudioSystem.getAudioInputStream(DeepSpeechClient.INPUT_FORMAT, sourceStream)

            val outputData = converted.readBytes()
            deepspeechClient?.add(outputData)
            if (!commandListeningEnabled && snowboyDetecter?.detect(outputData) == true) {
                context.penkaChannel.sendMessage("${userAudio.user.name} захотел голосовую команду.").queue()
                context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                        "Че надо ${userAudio.user.name}?", context.penkaGuild)

                // Starting recording...
                commandListeningEnabled = true;
                commandProviderID = userAudio.user.id
                commandListeningStartTime = System.currentTimeMillis()
                commandAudioData.clear()
                startRecognizer()
            }

            if (commandListeningEnabled) {
                if (System.currentTimeMillis() - commandListeningStartTime < 5000 &&
                    userAudio.user.id == commandProviderID) {
                    // Recording (todo: thing about performance)
                    outputData.forEach {
                        commandAudioData.add(it)
                    }
                }
            }
        }

        encodingSpeedCounter.push(processingTime.toDouble())
        sometimes.perform{
            logger.info{"Audio receive statistics: [processing: ${encodingSpeedCounter.getValue()}ms]"}
        }
    }

    private fun startRecognizer() {
        GlobalScope.launch {
            while (System.currentTimeMillis() - commandListeningStartTime < 5100) {
                delay(System.currentTimeMillis() - commandListeningStartTime)
            }

            commandListeningEnabled = false

            val recognized = yandexSpeechClient.recognizeSmall(
                    sampleRateHertz = 16000,
                    audioData = commandAudioData.toByteArray()
            )

            commandAudioData.clear()
            logger.info{ "Recognized: $recognized" }

            var found = false
            context.voiceCommandsContainer.buildCommands(context).forEach {
                if (it.hasFunctionsFor(recognized)) {
                    try {
                        it.execute(recognized)
                    } catch (t : Exception) {
                        context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                                "Пока делал что просили - что-то пошло не так. Иди смотреть логи.", context.penkaGuild)
                        logger.error{t.fillInStackTrace()}
                    }
                    found = true
                }
            }

            if (!found) {
                context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                        "Не могу понять че ты несешь сука", context.penkaGuild)
            }
        }
    }
}