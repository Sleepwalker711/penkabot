package ru.sleep7.listeners.voiceCommands

import ru.sleep7.features.implementations.MusicAudioManagers
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.features.implementations.PollyTextToSpeech
import ru.sleep7.listeners.abstracts.AbstractVoiceCommand
import ru.sleep7.penkabot.Context

class MusicPlayerVoiceCommand(context: Context) : AbstractVoiceCommand(context) {
    private val smallVolumeDelta = 10
    private val hugeVolumeDelta = 30

    init {
        addCommand("включи") { play(it) }
        addCommand("добавь") { play(it) }
        addCommand("пропусти") { skip() }
        addCommand("следующий трек") { skip() }
        addCommand("сделай потише") { changeVolume(-smallVolumeDelta) }
        addCommand("сделай сильно потише") { changeVolume(-hugeVolumeDelta) }
        addCommand("сделай погромче") { changeVolume(smallVolumeDelta) }
        addCommand("сделай сильно погромче") { changeVolume(hugeVolumeDelta) }
        addCommand("громкость") { setVolume(it) }
    }

    fun play(command: String) {
        val args = cutCommand(command, "включи")

        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .loadAndPlay(penkaContext.penkaGuild, "ytsearch:$args")

        say("Добавляю в очередь чё нашлось")
    }

    fun skip() {
        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .skip(penkaContext.penkaGuild)

        say("Пропускаю")
    }

    fun changeVolume(delta: Int) {
        penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(penkaContext.penkaGuild).player.volume += delta

        if (delta < 0) {
            say("Сделал потише")
        } else {
            say("Сделал погромче")
        }
    }

    fun setVolume(command: String) {
        val args = cutCommand(command, "громкость")

        penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(penkaContext.penkaGuild).player.volume = args.trim().toInt()

        say("Сделал")
    }

    fun say(text: String) {
        penkaContext.featuresContainer.findFeature<PollyTextToSpeech>()
                .say(text, penkaContext.penkaGuild)
    }
}