package ru.sleep7.listeners

import mu.KotlinLogging
import ru.sleep7.listeners.abstracts.AbstractVoiceCommand
import ru.sleep7.penkabot.Context
import ru.sleep7.utils.listClasses
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

private val logger = KotlinLogging.logger{}
class VoiceCommandsContainer {
    private var voiceCommandsClasses: ArrayList<KClass<*>> = listClasses<AbstractVoiceCommand>(
            "ru.sleep7.listeners.voiceCommands")

    private var commandsContainer: HashMap<String, AbstractVoiceCommand> = hashMapOf()

    fun buildCommands(context: Context): ArrayList<AbstractVoiceCommand> {

        val args = HashMap<String, Any?>().apply {
            put("context", context)
        }

        val result = ArrayList<AbstractVoiceCommand>()

        // If already loaded - just return it
        if (commandsContainer.isNotEmpty()) {
            commandsContainer.forEach {
                result.add(it.value)
            }

            return result
        }
        logger.info{"Building voice commands"}

        // Build instances
        voiceCommandsClasses.forEach { commandClass ->
            logger.info{"    Creating voice command \"${commandClass.simpleName}\""}
            val constructor = commandClass.primaryConstructor
            val arguments = HashMap<KParameter, Any?>().apply {
                constructor?.parameters?.forEach {
                    if (it.name in args) {
                        put(it, args[it.name])
                    }
                }
            }

            result.add(constructor?.callBy(arguments) as AbstractVoiceCommand)
        }

        result.forEach {
            commandsContainer[it::class.simpleName!!] = it
        }

        return result
    }
}