package ru.sleep7.listeners

import net.dv8tion.jda.api.audio.AudioReceiveHandler
import net.dv8tion.jda.api.audio.CombinedAudio
import net.dv8tion.jda.api.audio.OpusPacket
import net.dv8tion.jda.api.audio.UserAudio
import ru.sleep7.penkabot.Context

class DispatchAudioReceiveHandler(val penkaContext: Context) : AudioReceiveHandler {
    override fun canReceiveUser(): Boolean {
        // If at least one listener can receive
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveUser()) {
                return true;
            }
        }

        return false;
    }

    override fun canReceiveCombined(): Boolean {
        // If at least one listener can receive
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveCombined()) {
                return true;
            }
        }

        return false;
    }

    override fun canReceiveEncoded(): Boolean {
        // If at least one listener can receive
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveEncoded()) {
                return true;
            }
        }

        return false;
    }

    override fun handleUserAudio(userAudio: UserAudio) {
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveUser()) {
                it.handleUserAudio(userAudio)
            }
        }
    }

    override fun handleEncodedAudio(packet: OpusPacket) {
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveEncoded()) {
                it.handleEncodedAudio(packet)
            }
        }
    }

    override fun handleCombinedAudio(combinedAudio: CombinedAudio) {
        penkaContext.voiceListeners.buildListeners(penkaContext).forEach {
            if (it.canReceiveCombined()) {
                it.handleCombinedAudio(combinedAudio)
            }
        }
    }
}