package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext
import ru.sleep7.utils.parseToSeconds

open class MusicPlayerSeekCommand(context: Context) : AbstractCommandListener(context) {

    class CommandRequestBody(
        val exact: Int?,    // Seconds
        val forward: Int?,  // Seconds
        val backward: Int?  // Seconds
    )

    class RequestBody(
            val body: CommandRequestBody
    ) : BasicWebRequest()

    init {
        addChatCommand("seek") { args, event -> seekChat(args, event) }

        setRequestBodyType(RequestBody::class)
        addWebCommand("player/seek") { requestContext, request -> seekWeb(requestContext, request) }
    }

    private fun seekChat(args: String, event: MessageReceivedEvent) {
        val musicPlayer = penkaContext.featuresContainer.findFeature<MusicPlayer>();

        if (!musicPlayer.hasActiveTrack(event.guild)) {
            penkaContext.penkaChannel.sendMessage("Нечего перематывать.").queue()
            return
        }

        val result: Boolean;
        try {
            result = musicPlayer.seek(event.guild, parseSeekArguments(args, event))
        }
        catch (e: RuntimeException) {
            penkaContext.penkaChannel.sendMessage(e.message?: e.toString()).queue()
            return
        }

        if (!result) {
            penkaContext.penkaChannel.sendMessage("Нельзя проматывать текущий трек.").queue()
            return
        }
        penkaContext.penkaChannel.sendMessage("Промотано.").queue()
    }

    private fun seekWeb(requestContext: RequestContext, request: BasicWebRequest) : BasicWebResponse {
        val commandRequest = request as RequestBody
        val musicPlayer = penkaContext.featuresContainer.findFeature<MusicPlayer>();

        if (!musicPlayer.hasActiveTrack(requestContext.guild)) {
            return BasicWebResponse(code = -2, message = "No active track to seek")
        }

        if (commandRequest.body.exact != null) {
            if (!musicPlayer.seek(requestContext.guild, commandRequest.body.exact)) {
                return BasicWebResponse(code = -1, message = "Can't seek track")
            }
            return BasicWebResponse(message = "OK")
        }

        val currentPosition = musicPlayer.position(requestContext.guild)

        if (commandRequest.body.forward != null) {
            if (!musicPlayer.seek(requestContext.guild, currentPosition + commandRequest.body.forward)) {
                return BasicWebResponse(code = -1, message = "Can't seek track")
            }
            return BasicWebResponse(message = "OK")
        }

        if (commandRequest.body.backward != null) {
            if (!musicPlayer.seek(requestContext.guild, currentPosition - commandRequest.body.backward)) {
                return BasicWebResponse(code = -1, message = "Can't seek track")
            }
            return BasicWebResponse(message = "OK")
        }

        return BasicWebResponse(
            code = -1,
            message = "At least one option `exact`, `forward`, `backward` must be defined"
        )
    }

    private fun parseSeekArguments(args: String, event: MessageReceivedEvent): Int  {
        if (args.isEmpty()) {
            throw RuntimeException("Может быть аргументы укажешь, гений??")
        }

        val trimmed = args.trim()

        if (trimmed.startsWith('+')) {
            val parsed = parseToSeconds(trimmed.substring(1));

            if (parsed == -1) {
                throw RuntimeException("Не могу понять время.");
            }

            return penkaContext.featuresContainer.findFeature<MusicPlayer>()
                    .position(event.guild) + parsed
        } else if (trimmed.startsWith('-')) {
            val parsed = parseToSeconds(trimmed.substring(1));

            if (parsed == -1) {
                throw RuntimeException("Не могу понять время.");
            }

            return penkaContext.featuresContainer.findFeature<MusicPlayer>()
                    .position(event.guild) - parsed
        }

        val parsed = parseToSeconds(trimmed);

        if (parsed == -1) {
            throw RuntimeException("Не могу понять время.");
        }

        return parsed;
    }
}