package ru.sleep7.listeners.commands

import mu.KotlinLogging
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import org.joda.time.DateTime
import ru.sleep7.db.getCoubByUrl
import ru.sleep7.db.saveCoub
import ru.sleep7.features.implementations.CoubProcessor
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.Context

private val logger = KotlinLogging.logger{}

open class FetchCoubsCommand(context: Context) : AbstractCommandListener(context) {

    init {
        addChatCommand("fetch") { args, event -> fetch(args, event) }
    }

    private fun fetch(args: String, event: MessageReceivedEvent) {
        val argArray = args.split("\\s".toRegex(), 100)
        if (argArray.size != 2) {
            penkaContext.penkaChannel.sendMessage("fetch [channelID] [startMessageID]").queue()
            return
        }

        val fetchChannel = argArray[0]
        var maxMsgId = argArray[1].toLong()
        val coubProcessor = penkaContext.featuresContainer.findFeature<CoubProcessor>()

        while (true) {
            logger.info{"Starting fetching from message id: $maxMsgId"}
            val messages = penkaContext.jda.getTextChannelById(fetchChannel)?.
                    getHistoryAfter(maxMsgId, 100)?.
                    complete()?.
                    retrievedHistory

            if (messages?.isEmpty()!!) {
                logger.info{"Messages array is empty. End."}
                return
            }

            messages.forEach {
                if (it.idLong > maxMsgId) {
                    maxMsgId = it.idLong
                }

                if (coubProcessor.doesMessageContainsCoub(it)) {
                    val realUrl = coubProcessor.getCoubLink(it)
                    logger.info{"Found coub: $realUrl"}
                    getCoubByUrl(realUrl) ?: saveCoub("", realUrl, it.author.name, DateTime.now())
                }
            }
        }
    }
}