package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext

class MusicPlayerPauseCommand(context: Context) : AbstractCommandListener(context) {

    class PauseResponseBody(
        val status: String // `paused`/`unpaused`
    )

    class PauseResponse(
        val body: PauseResponseBody
    ) : BasicWebResponse(message = "OK")

    init {
        addChatCommand("pause") { _, event -> pauseChat(event) }

        setRequestBodyType(BasicWebRequest::class)
        addWebCommand("player/pause") { requestContext, _ -> pauseWeb(requestContext) }
    }

    private fun pauseChat(event: MessageReceivedEvent) {
        if (penkaContext.featuresContainer.findFeature<MusicPlayer>()
                        .pauseUnpause(event.guild)) {
            penkaContext.penkaChannel.sendMessage("Поставлено на паузу.").queue()
        } else {
            penkaContext.penkaChannel.sendMessage("Снято с паузы.").queue()
        }
    }

    private fun pauseWeb(requestContext: RequestContext) : BasicWebResponse {
        val result = penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .pauseUnpause(requestContext.guild)

        return PauseResponse(
            body = PauseResponseBody(
                status = if (result) "paused" else "unpaused"
            )
        )
    }
}