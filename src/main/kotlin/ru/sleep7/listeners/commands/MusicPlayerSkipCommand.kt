package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext

class MusicPlayerSkipCommand(context: Context) : AbstractCommandListener(context) {
    init {
        addChatCommand("skip") { _, event -> skipChat(event) }
        addChatCommand("s") { _, event -> skipChat(event) }

        setRequestBodyType(BasicWebRequest::class)
        addWebCommand("player/skip") { requestContext, _ -> skipWeb(requestContext) }
    }

    private fun skipChat(event: MessageReceivedEvent) {
        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .skip(event.guild)

        penkaContext.penkaChannel.sendMessage("Пропущено.").queue()
    }

    private fun skipWeb(requestContext: RequestContext) : BasicWebResponse {
        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .skip(requestContext.guild)

        return BasicWebResponse(message = "OK")
    }
}