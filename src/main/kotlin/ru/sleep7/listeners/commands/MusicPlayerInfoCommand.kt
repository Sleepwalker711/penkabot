package ru.sleep7.listeners.commands

import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext
import ru.sleep7.utils.secondToDateString

class MusicPlayerInfoCommand(context: Context) : AbstractCommandListener(context) {

    class InfoMusic(
        val name: String,
        val link: String,
        val duration: Long, // seconds
        val cursor: Long?
    )

    class InfoResponseBody(
        val currentlyPlaying: InfoMusic?,
        val playlist: ArrayList<InfoMusic>
    )

    class InfoResponse(
        val body: InfoResponseBody
    ) : BasicWebResponse(message = "OK")

    init {
        addChatCommand("info") { _, event -> infoChat(event) }

        setRequestBodyType(BasicWebRequest::class)
        addWebCommand("player/info") { requestContext, request -> infoWeb(requestContext, request) }
    }

    private fun infoChat (event: MessageReceivedEvent) {
        val info = penkaContext.featuresContainer.findFeature<MusicPlayer>().buildEmbedInfo(event.guild)
        penkaContext.penkaChannel.sendMessage(info).queue()
    }

    private fun infoWeb(requestContext: RequestContext, request: BasicWebRequest) : BasicWebResponse {
        val player = penkaContext.featuresContainer.findFeature<MusicPlayer>()
        val trackList = player.trackList(requestContext.guild)
        val currentTrack = player.currentPlayingTrack(requestContext.guild)

        var currentTrackMusic: InfoMusic? = null
        if (currentTrack != null) {
            currentTrackMusic = InfoMusic(
                name = player.fetchTrackInfoTitle(currentTrack),
                link = currentTrack.info.uri,
                duration = currentTrack.duration,
                cursor = currentTrack.position
            )
        }

        val trackListMusic = ArrayList<InfoMusic>(trackList.size)
        trackList.forEach {track ->
            trackListMusic.add(InfoMusic(
                name = player.fetchTrackInfoTitle(track),
                link = track.info.uri,
                duration = track.duration,
                cursor = track.position
            ))
        }

        return InfoResponse(
            body = InfoResponseBody(
                currentlyPlaying = currentTrackMusic,
                playlist = trackListMusic
            )
        )
    }
}