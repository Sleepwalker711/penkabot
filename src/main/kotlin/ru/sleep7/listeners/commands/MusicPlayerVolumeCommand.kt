package ru.sleep7.listeners.commands

import ru.sleep7.features.implementations.MusicAudioManagers
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext

class MusicPlayerVolumeCommand(context: Context) : AbstractCommandListener(context) {

    class CommandRequestBody(
        val increase: Int?,
        val decrease: Int?
    )

    class RequestBody(
        val body: CommandRequestBody
    ) : BasicWebRequest()

    class VolumeResponseBody(
        val volume: Int
    )

    class VolumeResponse(
        val body: VolumeResponseBody
    ) : BasicWebResponse(message = "OK")

    init {
        penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(penkaContext.penkaGuild).player.volume = context.config.basic.default_music_volume

        addChatCommand("volume") { args, _ -> volumeChat(args) }
        addChatCommand("v") { args, _ -> volumeChat(args) }

        setRequestBodyType(RequestBody::class)
        addWebCommand("player/volume") { requestContext, request -> volumeWeb(requestContext, request)}
    }

    private fun volumeChat(args: String) {
        val audioManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(penkaContext.penkaGuild)

        if (args.isNotEmpty()) {
            if (args.startsWith("+") || args.startsWith("-")) {
                val volumeDelta = args.toInt()
                audioManager.player.volume += volumeDelta
            } else {
                audioManager.player.volume = args.toInt()
            }
        }

        penkaContext.penkaChannel.sendMessage("Громкость: ${audioManager.player.volume}").queue()
    }

    private fun volumeWeb(requestContext: RequestContext, request: BasicWebRequest) : BasicWebResponse {
        val commandRequest = request as RequestBody

        val audioManager = penkaContext.featuresContainer.findFeature<MusicAudioManagers>()
                .getGuildAudioManager(requestContext.guild)

        if (commandRequest.body.increase != null) {
            audioManager.player.volume += commandRequest.body.increase
        }

        if (commandRequest.body.decrease != null) {
            audioManager.player.volume -= commandRequest.body.decrease
        }

        return VolumeResponse(
            body = VolumeResponseBody(
                volume = audioManager.player.volume
            )
        )
    }
}