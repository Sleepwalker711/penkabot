package ru.sleep7.listeners.commands

import com.amazonaws.services.polly.model.VoiceId
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.PollyTextToSpeech
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.*

class TTSSpecialCommand(val context: Context) : AbstractCommandListener(context) {

    init {
        addChatCommand("ttsspecial") { args, event -> ttsChat(args, event) }
    }

    private fun ttsChat(args: String, event: MessageReceivedEvent) {
        context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                text=args,
                guild=event.guild,
                voice = VoiceId.Tatyana
        )
    }
}