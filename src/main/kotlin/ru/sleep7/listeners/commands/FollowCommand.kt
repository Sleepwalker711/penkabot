package ru.sleep7.listeners.commands

import mu.KotlinLogging
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.FollowProcessor
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext

private val logger = KotlinLogging.logger{}

class FollowCommand(context: Context) : AbstractCommandListener(context) {

    class FollowRequestBody(
        val name: String
    )

    class RequestBody(
        val body: FollowRequestBody
    ) : BasicWebRequest()

    init {
        addChatCommand("follow") { args, event -> followChat(args, event) }

        setRequestBodyType(RequestBody::class)
        addWebCommand("bot/follow") { requestContext, request -> followWeb(requestContext, request) }
    }

    private fun followChat(args: String, event: MessageReceivedEvent) {
        var name = args;
        if (args.equals("me", true)) {
            name = event.author.name
        }

        followByName(name)

        penkaContext.featuresContainer.findFeature<FollowProcessor>().joinToOwner()
    }

    private fun followWeb(requestContext: RequestContext, request: BasicWebRequest) : BasicWebResponse {
        val commandRequest = request as RequestBody

        followByName(commandRequest.body.name)

        return BasicWebResponse(message = "OK")
    }

    private fun followByName(name: String) {
        penkaContext.targetUser = penkaContext.jda.guilds.first().members.map { it.user }.first {
            it.name.equals(name, true)
        }

        logger.info { "User [id: ${penkaContext.targetUser.id}, name: ${penkaContext.targetUser.name}] requested follow" }
    }
}