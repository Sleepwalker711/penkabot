package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.Context

class DumbMessageCommand(context: Context) : AbstractCommandListener(context) {
    init {
        addChatCommand("post_dumb_message") { _, _ -> postDumbMessage() }
    }

    private fun postDumbMessage() {
        penkaContext.penkaChannel.sendMessage("Тупое сообщение").queue()
    }
}