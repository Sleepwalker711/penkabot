package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.PollyTextToSpeech
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.*

class TTSCommand(val context: Context) : AbstractCommandListener(context) {

    class CommandRequestBody(val text: String)
    class RequestBody(
            val body: CommandRequestBody
    ) : BasicWebRequest()

    init {
        addChatCommand("tts") { args, event -> ttsChat(args, event) }

        setRequestBodyType(RequestBody::class)
        addWebCommand("bot/tts") {requestContext, request -> ttsWeb(requestContext, request) }
    }

    fun ttsChat(args: String, event: MessageReceivedEvent) {
        context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                text=args,
                guild=event.guild
        )
    }

    fun ttsWeb(requestContext: RequestContext, request: BasicWebRequest): BasicWebResponse {
        val commandRequest = request as RequestBody

        this.context.featuresContainer.findFeature<PollyTextToSpeech>().say(
                text=commandRequest.body.text,
                guild=requestContext.guild
        )

        return BasicWebResponse(message = "OK")
    }
}