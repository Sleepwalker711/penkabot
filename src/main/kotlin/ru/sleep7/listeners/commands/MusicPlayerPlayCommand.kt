package ru.sleep7.listeners.commands

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import ru.sleep7.features.implementations.MusicPlayer
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext

open class MusicPlayerPlayCommand(context: Context) : AbstractCommandListener(context) {

    class CommandRequestBody(
            val path: String
    )

    class RequestBody(
            val body: CommandRequestBody
    ) : BasicWebRequest()

    init {
        addChatCommand("play") { args, event -> playChat(args, event) }
        addChatCommand("p") { args, event -> playChat(args, event) }
        addChatCommand("pyt") { args, event -> playYoutubeChat(args, event) }

        setRequestBodyType(RequestBody::class)
        addWebCommand("player/play") { requestContext, request -> playWeb(requestContext, request) }
    }

    private fun playChat(args: String, event: MessageReceivedEvent) {
        val musicPlayer = penkaContext.featuresContainer.findFeature<MusicPlayer>()

        if (args.isEmpty()) {
            musicPlayer.unpause(event.guild)
            return;
        }

        musicPlayer.loadAndPlay(event.guild, args)
    }

    private fun playYoutubeChat(args: String, event: MessageReceivedEvent) {
        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .loadAndPlay(event.guild, "ytsearch:$args")
    }

    private fun playWeb(requestContext: RequestContext, request: BasicWebRequest) : BasicWebResponse {
        val commandRequest = request as RequestBody

        penkaContext.featuresContainer.findFeature<MusicPlayer>()
                .loadAndPlay(requestContext.guild, commandRequest.body.path)

        return BasicWebResponse(message = "OK")
    }
}