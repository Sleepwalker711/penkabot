package ru.sleep7.listeners.abstracts

import ru.sleep7.penkabot.Context
import java.lang.RuntimeException

abstract class AbstractVoiceCommand(val penkaContext: Context) {
    private val commands = mutableMapOf<String, (String) -> Unit>()
    protected fun addCommand(command: String, block: (String) -> Unit) {
        commands[command.toLowerCase()] = block
    }

    fun hasFunctionsFor(command: String): Boolean {
        commands.keys.forEach {
            if (command.toLowerCase().startsWith(it)) {
                return true
            }
        }

        return false
    }

    fun execute(command: String) {
        commands.keys.forEach {
            if (command.toLowerCase().startsWith(it)) {
                commands[it]?.invoke(command)
                return
            }
        }

        throw RuntimeException("Class said that is can provide function for command $command, but it's not")
    }

    protected fun cutCommand(command: String, name: String): String {
        return command.substring(name.length)
    }
}