package ru.sleep7.listeners.abstracts

import net.dv8tion.jda.api.entities.ChannelType
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import ru.sleep7.penkabot.BasicWebRequest
import ru.sleep7.penkabot.BasicWebResponse
import ru.sleep7.penkabot.Context
import ru.sleep7.penkabot.RequestContext
import kotlin.reflect.KClass


abstract class AbstractCommandListener(val penkaContext: Context) : ListenerAdapter() {
    private var requestBody: KClass<*>? = null
    private val commands = mutableMapOf<String, (String, MessageReceivedEvent) -> Unit>()
    private val webCommands = mutableMapOf<String, (RequestContext, BasicWebRequest) -> BasicWebResponse>()

    protected fun addChatCommand(command: String, block: (String, MessageReceivedEvent) -> Unit) {
        commands[command.toLowerCase()] = block
    }

    protected fun addWebCommand(command: String, block: (RequestContext, BasicWebRequest) -> BasicWebResponse) {
        webCommands[command.toLowerCase()] = block
    }

    protected fun setRequestBodyType(req: KClass<*>) {
        requestBody = req
    }

    fun getRequestBodyType(): KClass<*>? {
        return requestBody;
    }

    fun getWebCommands(): MutableSet<String> {
        return webCommands.keys
    }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (!event.isFromType(ChannelType.TEXT)) {
            return
        }

        if (event.message.author.isBot) {
            return
        }

        val splittedMessage = event.message.contentRaw.split(" ")

        if (!splittedMessage[0].startsWith(penkaContext.config.basic.commands_prefix)) {
            if (commands.keys.contains("ttsspecial")) {
                if (event.channel.id == penkaContext.config.discord.ids.tts_text_channel) {
                    commands["ttsspecial"]?.invoke(event.message.contentRaw, event)
                    event.channel.deleteMessageById(event.messageId).queue()
                    return
                }
            }
            return

        }

        val command = splittedMessage[0].removePrefix(penkaContext.config.basic.commands_prefix)
        var args: String = event.message.contentRaw.replace(splittedMessage[0], "")
        if (args.isNotEmpty()) {
            args = args.substring(1, args.length)
        }

        if (commands.keys.contains(command.toLowerCase())) {
            if (event.textChannel.id != penkaContext.penkaChannel.id) {
                penkaContext.penkaChannel.sendMessage(event.author.name + ": " + event.message.contentDisplay).queue()
                event.channel.deleteMessageById(event.messageId).queue()
            }

            commands[command.toLowerCase()]?.invoke(args, event)
        }
    }

    open fun onJsonRequest(requestContext: RequestContext, body: BasicWebRequest): BasicWebResponse {
        val lowerCommand = requestContext.command.toLowerCase()

        if (!webCommands.keys.contains(lowerCommand)) {
            return BasicWebResponse(
                    code = -1,
                    message = "No web processor for ${requestContext.command} command"
            )
        }

        return webCommands[lowerCommand]?.invoke(requestContext, body) ?: BasicWebResponse(message = "OK")
    }
}