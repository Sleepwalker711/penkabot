package ru.sleep7.listeners.abstracts

import net.dv8tion.jda.api.audio.AudioReceiveHandler

interface AbstractSimpleVoiceListener : AudioReceiveHandler {

    fun isEnabled(): Boolean {
        return true;
    }

}