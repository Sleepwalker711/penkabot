package ru.sleep7.listeners

import mu.KotlinLogging
import net.dv8tion.jda.api.hooks.ListenerAdapter
import ru.sleep7.listeners.abstracts.AbstractCommandListener
import ru.sleep7.penkabot.Context
import ru.sleep7.utils.listClasses
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

private val logger = KotlinLogging.logger {}
class ListenersContainer {

    private var commandListeners: ArrayList<KClass<*>> = listClasses<AbstractCommandListener>(
            "ru.sleep7.listeners.commands")
    private var simpleListenersClasses: ArrayList<KClass<*>> = listClasses<ListenerAdapter>(
            "ru.sleep7.listeners.simpleListeners")

    private var listenersContainer: HashMap<String, ListenerAdapter> = hashMapOf()

    fun buildCommandListeners(context: Context): ArrayList<AbstractCommandListener> {
        if (listenersContainer.isEmpty()) {
            buildListeners(context)
        }

        val result = ArrayList<AbstractCommandListener>()

        commandListeners.forEach {
            result.add(listenersContainer[it.simpleName] as AbstractCommandListener)
        }

        return result;
    }

    fun buildListeners(context: Context): ArrayList<ListenerAdapter> {
        val args = HashMap<String, Any?>().apply {
            put("context", context)
        }

        val result = ArrayList<ListenerAdapter>()

        // If already loaded - just return it
        if (listenersContainer.isNotEmpty()) {
            listenersContainer.forEach {
                result.add(it.value)
            }

            return result
        }

        logger.info{ "Building listeners"}

        // Build instances
        simpleListenersClasses.forEach { commandClass ->
            if (context.config.listeners.enabled_listeners.contains(commandClass.simpleName)) {
                logger.info{"    Creating simple listener \"${commandClass.simpleName}\""}
                val constructor = commandClass.primaryConstructor
                val arguments = HashMap<KParameter, Any?>().apply {
                    constructor?.parameters?.forEach {
                        if (it.name in args) {
                            put(it, args[it.name])
                        }
                    }
                }

                result.add(constructor?.callBy(arguments) as ListenerAdapter)
            } else {
                logger.info("    Simple listener \"${commandClass.simpleName}\" is not enabled")
            }
        }

        commandListeners.forEach { commandClass ->
            if (context.config.listeners.enabled_listeners.contains(commandClass.simpleName)) {
                logger.info { "    Creating chat command \"${commandClass.simpleName}\"" }
                val constructor = commandClass.primaryConstructor
                val arguments = HashMap<KParameter, Any?>().apply {
                    constructor?.parameters?.forEach {
                        if (it.name in args) {
                            put(it, args[it.name])
                        }
                    }
                }

                result.add(constructor?.callBy(arguments) as ListenerAdapter)
            } else {
                logger.info("    Chat command \"${commandClass.simpleName}\" is not enabled")
            }
        }

        // Fill hash
        result.forEach {
            listenersContainer[it::class.simpleName!!] = it
        }

        logger.info{"Built ${result.size} listeners successfully"}

        return result
    }

    fun findListener(name: String): ListenerAdapter? {
        if (listenersContainer.isEmpty()) {
            throw RuntimeException("Chat commands container was not initalized")
        }

        return listenersContainer[name]
    }

    inline fun <reified T> findListener(): T {
        return findListener(T::class.simpleName!!) as T
    }

}