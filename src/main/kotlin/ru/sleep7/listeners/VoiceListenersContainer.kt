package ru.sleep7.listeners

import mu.KotlinLogging
import ru.sleep7.listeners.abstracts.AbstractSimpleVoiceListener
import ru.sleep7.penkabot.Context
import ru.sleep7.utils.listClasses
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

private val logger = KotlinLogging.logger{}

class VoiceListenersContainer {
    private val simpleVoiceListeners: ArrayList<KClass<*>> = listClasses<AbstractSimpleVoiceListener>(
            "ru.sleep7.listeners.simpleVoiceListeners")
    
    private var listenersContainer: HashMap<String, AbstractSimpleVoiceListener> = hashMapOf()
    private var alreadyBuilt = false;
    
    fun buildListeners(context: Context): ArrayList<AbstractSimpleVoiceListener> {
        val args = HashMap<String, Any?>().apply {
            put("context", context)
        }

        val result = ArrayList<AbstractSimpleVoiceListener>()

        // If already loaded - just return it
        if (alreadyBuilt) {
            listenersContainer.forEach {
                result.add(it.value)
            }

            return result
        }
        
        logger.info{ "Building voice listeners"}

        // Build instances
        simpleVoiceListeners.forEach { commandClass ->
            logger.info{"    Creating simple listener \"${commandClass.simpleName}\""}
            val constructor = commandClass.primaryConstructor
            val arguments = HashMap<KParameter, Any?>().apply {
                constructor?.parameters?.forEach {
                    if (it.name in args) {
                        put(it, args[it.name])
                    }
                }
            }

            val instance = constructor?.callBy(arguments) as AbstractSimpleVoiceListener

            if (instance.isEnabled()) {
                result.add(instance)
            } else {
                logger.info{"        \"${commandClass.simpleName}\" is disabled"}
            }
        }

        // Fill hash
        result.forEach {
            listenersContainer[it::class.simpleName!!] = it
        }
        alreadyBuilt = true;
        logger.info{"Built ${result.size} audio listeners successfully"}
        
        return result
    }
}