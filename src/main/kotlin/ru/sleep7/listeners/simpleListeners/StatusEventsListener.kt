package ru.sleep7.listeners.simpleListeners

import mu.KotlinLogging
import net.dv8tion.jda.api.entities.MessageType
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import ru.sleep7.features.implementations.StatusManager
import ru.sleep7.penkabot.Context

val logger = KotlinLogging.logger{}

class StatusEventsListener(val context: Context) : ListenerAdapter() {
    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.channel.idLong != context.penkaChannel.idLong) {
            return
        }

        logger.info{ event.message.type }
        if (event.message.type == MessageType.CHANNEL_PINNED_ADD) {
            context.penkaChannel.deleteMessageById(event.messageId).queue()
        }
    }
}