package ru.sleep7.listeners.simpleListeners

import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.joda.time.DateTime
import ru.sleep7.db.getCoubByUrl
import ru.sleep7.db.saveCoub
import ru.sleep7.features.implementations.CoubProcessor
import ru.sleep7.penkabot.Context

open class CoubSaverListener(val context: Context) : ListenerAdapter() {

    override fun onMessageReceived(event: MessageReceivedEvent) {
        val coubProcessor = context.featuresContainer.findFeature<CoubProcessor>()

        if (!event.author.isBot && coubProcessor.doesMessageContainsCoub(event.message)) {
            val coubUrl = coubProcessor.getCoubLink(event.message)
            getCoubByUrl(coubUrl) ?: saveCoub("", coubUrl, event.author.name, DateTime.now())

            if (event.message.channel.idLong != context.coubChannel.idLong) {
                context.coubChannel.sendMessage("${event.author.name}: $coubUrl").queue()
                event.channel.deleteMessageById(event.messageId).queue()
            }
        }
    }

}