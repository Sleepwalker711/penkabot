package ru.sleep7.listeners.simpleListeners

import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.voice.*
import net.dv8tion.jda.api.hooks.ListenerAdapter
import ru.sleep7.features.implementations.PollyTextToSpeech
import ru.sleep7.penkabot.Context

class MemberStatusChangeNotificationListener(val context: Context) : ListenerAdapter() {
    override fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {
        if (event.channelJoined.id == context.currentVoiceChannel.id &&
            !event.member.user.isBot &&
            event.member.user.id != context.targetUser.id) {
            val message = getStringFrom(
                    strings=context.config.member_status_notification.messages.on_move_in,
                    member=event.member
            ) ?: return

            context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
        }

        if (event.channelLeft.id == context.currentVoiceChannel.id &&
            !event.member.user.isBot &&
            event.member.user.id != context.targetUser.id) {
            val message = getStringFrom(
                    strings=context.config.member_status_notification.messages.on_move_out,
                    member=event.member
            ) ?: return

            context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
        }
    }

    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        if (event.channelJoined.id != context.currentVoiceChannel.id ||
            event.member.user.isBot) {
            return
        }

        val message = getStringFrom(
                strings=context.config.member_status_notification.messages.on_join,
                member=event.member
        ) ?: return

        context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
    }

    override fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {
        if (event.channelLeft.id != context.currentVoiceChannel.id ||
            event.member.user.isBot) {
            return
        }

        val message = getStringFrom(
                strings=context.config.member_status_notification.messages.on_leave,
                member=event.member
        ) ?: return

        context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
    }

    override fun onGuildVoiceMute(event: GuildVoiceMuteEvent) {
        if (event.guild.idLong != context.penkaGuild.idLong ||
            event.member.user.isBot ||
            event.voiceState.isDeafened) {
            return
        }

        val message = if (event.isMuted) {
            getStringFrom(
                    strings=context.config.member_status_notification.messages.on_mute,
                    member=event.member
            ) ?: return
        } else {
            getStringFrom(
                    strings=context.config.member_status_notification.messages.on_unmute,
                    member=event.member
            ) ?: return
        }

        context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
    }

    override fun onGuildVoiceDeafen(event: GuildVoiceDeafenEvent) {
        if (event.guild.idLong != context.penkaGuild.idLong ||
            event.member.user.isBot) {
            return
        }

        val message = if (event.isDeafened) {
            getStringFrom(
                    strings=context.config.member_status_notification.messages.on_deaf,
                    member=event.member
            ) ?: return
        } else {
            getStringFrom(
                    strings=context.config.member_status_notification.messages.on_hear,
                    member=event.member
            ) ?: return
        }

        context.featuresContainer.findFeature<PollyTextToSpeech>().say(text=message, guild=event.guild)
    }

    private fun getMemberName(member: Member): String {
        return member.nickname ?: member.user.name
    }

    fun getStringFrom(strings: List<String>, member: Member): String? {
        if (strings.isEmpty()) {
            return null;
        }

        val str = selectRandom(strings);

        return str.replace("{NAME}", getMemberName(member));
    }

    fun selectRandom(strings: List<String>): String {
        return strings.shuffled().take(1)[0]
    }
}