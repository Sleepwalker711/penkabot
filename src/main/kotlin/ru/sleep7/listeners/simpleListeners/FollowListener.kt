package ru.sleep7.listeners.simpleListeners

import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import ru.sleep7.features.implementations.FollowProcessor
import ru.sleep7.penkabot.Context

class FollowListener(val context: Context) : ListenerAdapter() {
    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        context.featuresContainer.findFeature<FollowProcessor>().joinToOwner()
    }

    override fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {
        context.featuresContainer.findFeature<FollowProcessor>().joinToOwner()
    }
}