package ru.sleep7.clients

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import io.ktor.client.HttpClient
import io.ktor.client.request.header
import io.ktor.client.request.post
import mu.KotlinLogging
import ru.sleep7.configuration.YandexConfig

private val logger = KotlinLogging.logger{}

class YandexSpeechAPI(val config: YandexConfig) {
    enum class Format(val internal_name: String) {
        LPCM("lpcm"),
        OGGOPUS("oggopus"),
    }

    enum class Language(val internal_name: String) {
        RU("ru-RU"),
        EN("en-US"),
        TR("tr-TR"),
    }

    enum class Voice(val internal_name: String) {
        ALYSS("alyss"),
        JANE("jane"),
        OKSANA("oksana"),
        OMAZH("omazh"),

        ZAHAR("zahar"),
        ERMIL("ermil"),
    }

    enum class Emotion(val internal_name: String) {
        GOOD("good"),
        EVIL("evil"),
        NEUTRAL("neutral"),
    }

    private val iamBaseUrl = "https://iam.api.cloud.yandex.net"
    private val baseUrl = "https://stt.api.cloud.yandex.net"
    private val client = HttpClient{}

    private var lastTokenRequest = 0L
    private var iamToken = ""

    suspend fun textToSpeech(text: String = "",
                             ssml: String = "",
                             lang: Language = Language.RU,
                             voice: Voice = Voice.OKSANA,
                             emotion: Emotion = Emotion.NEUTRAL,
                             speed: Float = 1.0f,
                             format: Format = Format.LPCM,
                             sampleRateHertz: Int = 48000) {
        val client = HttpClient()

        val response = client.post<String>("$baseUrl/speech/v1/tts:synthesize")
    }

    suspend fun recognizeSmall(lang: Language = Language.RU,
                               topic: String = "general",
                               profanityFilter: Boolean = false,
                               format: Format = Format.LPCM,
                               sampleRateHertz: Int = 48000,
                               audioData: ByteArray): String {
        val client = HttpClient()

        val response = client.post<String>("$baseUrl/speech/v1/stt:recognize") {
            url {
                parameters.append("lang", lang.internal_name)
                parameters.append("topic", topic)
                parameters.append("profanityFilter", profanityFilter.toString())
                parameters.append("format", format.internal_name)
                parameters.append("sampleRateHertz", sampleRateHertz.toString())
                parameters.append("folderId", config.folder_id)
            }

            header("Authorization", "Bearer ${getIAMToken()}")
            body = audioData
        }

        val parser = Parser.default()
        val stringBuilder = StringBuilder(response)
        val json = parser.parse(stringBuilder) as JsonObject

        return json.string("result")!!
    }

    private suspend fun getIAMToken() : String {
        if (System.currentTimeMillis() < lastTokenRequest + 60 * 60 * 1000) {
            return iamToken;
        }
        val response = client.post<String>("$iamBaseUrl/iam/v1/tokens") {
            body = "{\"yandexPassportOauthToken\":\"${config.token}\"}"
        }

        val parser = Parser.default()
        val stringBuilder = StringBuilder(response)
        val json = parser.parse(stringBuilder) as JsonObject

        lastTokenRequest = System.currentTimeMillis()
        iamToken = json.string("iamToken")!!

        logger.info{ "New IAM token requested successfully"}

        return iamToken
    }
}