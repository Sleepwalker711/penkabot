package ru.sleep7.clients

import io.ktor.client.HttpClient
import ru.sleep7.configuration.DeepSpeechConfig
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.ws
import io.ktor.http.HttpMethod
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.io.core.ByteReadPacket
import mu.KotlinLogging
import org.apache.commons.collections4.queue.CircularFifoQueue
import ru.sleep7.utils.Sometimes
import java.util.concurrent.atomic.AtomicInteger
import javax.sound.sampled.AudioFormat

private val logger = KotlinLogging.logger{}

class DeepSpeechClient(config: DeepSpeechConfig) {
    private val client = HttpClient {
        install(WebSockets)
    }

    companion object {
        val INPUT_FORMAT = AudioFormat(16000.0f, 16, 1, true, false)
    }

    private val sendQueue = CircularFifoQueue<ByteArray>((INPUT_FORMAT.frameRate * INPUT_FORMAT.frameSize * 5 /* seconds */).toInt() / 560)
    private var lastReceivedFrameTime: Long = System.currentTimeMillis()
    private var requiredUserFramesCounter = AtomicInteger(config.required_user_frames_to_send)
    private var sometimes = Sometimes(1.0, true)

    init {
        GlobalScope.launch {
            while (true) {
                try {
                    client.ws (
                            method = HttpMethod.Get,
                            host = config.host,
                            port = config.port,
                            path = config.path
                    ) {
                        while (true) {
                            try {
                                sometimes.perform {
                                    logger.info {"Sending statistics: [frame_counter: $requiredUserFramesCounter, queue_size: ${sendQueue.size}]"}
                                }
                                if (requiredUserFramesCounter.get() > 0 || sendQueue.size < config.minimum_frames_to_send) {
                                    var diff = System.currentTimeMillis() - lastReceivedFrameTime

                                    if (diff > 20) {
                                        while (diff > 20) {
                                            diff -= 20
                                            if (sendQueue.isNotEmpty()) {
                                                sendQueue.remove()
                                            }
                                        }

                                        lastReceivedFrameTime = System.currentTimeMillis()
                                    }

                                    delay(config.check_delay_ms)
                                    continue
                                }

                                var sent = 0
                                sendQueue.forEach {
                                    send(Frame.Binary(true, ByteReadPacket(it)))
                                    sent += it.size
                                }

                                requiredUserFramesCounter.set(config.required_user_frames_to_send)
                                send(Frame.Text("EOS"))
                                logger.info { "Receiving info [sent: $sent]" }
                                when (val frame = incoming.receive()) {
                                    is Frame.Text -> logger.info{"Received response: \"${frame.readText()}\""}
                                }
                            }
                            catch (t : Exception) {
                                logger.error { "Excepting received during sending to deepspeech: $t" }
                                break
                            }
                        }
                    }
                }
                catch (t : Exception) {
                    logger.error { "Exception received during connecting to deepspeech: $t" }
                }

                logger.info{ "Disconnected from deepspeech server. Retry in ${config.reconnect_time_ms}"}
                delay(config.reconnect_time_ms)
            }
        }
    }

    fun add(pcm: ByteArray) {
        sendQueue.add(pcm)
        requiredUserFramesCounter.decrementAndGet()
        lastReceivedFrameTime = System.currentTimeMillis()
    }
}