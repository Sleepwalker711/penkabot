package ru.sleep7.penkabot

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.requests.GatewayIntent
import ru.sleep7.configuration.Config
import ru.sleep7.listeners.DispatchAudioReceiveHandler

class PenkaBot(config: Config) {
    var jda: JDA
    var context: Context
    var webService: WebService

    init {
        jda = initJDA(config)
        context = Context(jda, config)
        webService = WebService(context)

        preparePlayerManager()
        listenersRegistration(context)
        configureJDA(context)

        jda.run{}

        context.featuresContainer.performInitialization()
    }

    fun start() {
        webService.runAndWait()
    }

    private fun initJDA(config: Config): JDA {
        val jda = JDABuilder.create(
                config.discord.api_token,
                listOf(
                        GatewayIntent.GUILD_BANS,
                        GatewayIntent.GUILD_EMOJIS,
                        GatewayIntent.GUILD_INVITES,
                        GatewayIntent.GUILD_VOICE_STATES,
                        GatewayIntent.GUILD_MESSAGES,
                        GatewayIntent.GUILD_MESSAGE_REACTIONS,
                        GatewayIntent.DIRECT_MESSAGES,
                        GatewayIntent.DIRECT_MESSAGE_REACTIONS
                )
        ).build()

        jda.awaitReady()

        return jda
    }

    private fun configureJDA(context: Context) {
        context.penkaGuild.audioManager.receivingHandler = context.audioReceiveHandler
        context.penkaGuild.audioManager.openAudioConnection(context.voiceChannel)
    }

    private fun preparePlayerManager() {
        AudioSourceManagers.registerRemoteSources(context.playerManager)
        AudioSourceManagers.registerLocalSource(context.playerManager)
    }

    private fun listenersRegistration(context: Context) {
        context.listenersContainer.buildListeners(context).forEach{
            jda.addEventListener(it)
        }
    }
}

