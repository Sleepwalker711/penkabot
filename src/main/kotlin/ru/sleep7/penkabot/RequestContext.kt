package ru.sleep7.penkabot

import net.dv8tion.jda.api.entities.Guild

class RequestContext(
        val commonData: CommonData,
        val guild: Guild,
        val command: String
)