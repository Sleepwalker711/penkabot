package ru.sleep7.penkabot

import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import mu.KotlinLogging

open class BasicWebResponse(val code: Int = 0, val message: String = "")

class CommonData {
    val author: String? = null
}
open class BasicWebRequest {
    val common: CommonData = CommonData()
}

private val logger = KotlinLogging.logger{}
class WebService(private val penkaContext: Context) {
    private var server: ApplicationEngine

    init {
        server = embeddedServer(Netty, penkaContext.config.basic.web_service_port) {
            install(ContentNegotiation) {
                gson{

                }
            }

            routing {
                penkaContext.listenersContainer.buildCommandListeners(penkaContext).forEach { listener ->
                    listener.getWebCommands().forEach { command ->
                        logger.info{ "Installing `/command/$command` handler for ${listener.javaClass.simpleName} controller" }
                        post("/command/$command") {
                            val requestType = listener.getRequestBodyType()

                            if (requestType == null) {
                                call.respond(
                                        HttpStatusCode.BadRequest,
                                        BasicWebResponse(message="No request type for command $command")
                                )
                                return@post
                            }

                            try {
                                val request = call.receive(requestType) as BasicWebRequest

                                val requestContext = RequestContext(
                                        commonData = request.common,
                                        guild = penkaContext.penkaGuild,
                                        command = command
                                )

                                call.respond(listener.onJsonRequest(requestContext, request))
                            }
                            catch (e: Exception) {
                                call.respond(
                                        HttpStatusCode.InternalServerError,
                                        BasicWebResponse(message=e.message ?: e.javaClass.simpleName)
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    fun runAndWait() {
        server.start(wait = true)
    }
}