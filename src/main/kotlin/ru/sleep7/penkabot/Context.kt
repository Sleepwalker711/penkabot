package ru.sleep7.penkabot

import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats
import com.sedmelluq.discord.lavaplayer.player.AudioConfiguration
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User
import net.dv8tion.jda.api.entities.VoiceChannel
import ru.sleep7.configuration.Config
import ru.sleep7.features.FeaturesContainer
import ru.sleep7.listeners.DispatchAudioReceiveHandler
import ru.sleep7.listeners.ListenersContainer
import ru.sleep7.listeners.VoiceCommandsContainer
import ru.sleep7.listeners.VoiceListenersContainer

class Context(val jda: JDA, val config: Config) {
    var penkaGuild: Guild = jda.getGuildById(config.discord.ids.main_guild)!!
    var targetUser: User = jda.getUserById(config.discord.ids.default_main_user)!!
    var penkaChannel: TextChannel = jda.getTextChannelById(config.discord.ids.main_text_channel)!!
    var voiceChannel: VoiceChannel = jda.getVoiceChannelById(config.discord.ids.default_voice_channel)!!
    var coubChannel: TextChannel = jda.getTextChannelById(config.discord.ids.coub_text_channel)!!
    var playerManager = DefaultAudioPlayerManager()
    var audioReceiveHandler = DispatchAudioReceiveHandler(this)

    var currentVoiceChannel: VoiceChannel = voiceChannel

    val listenersContainer = ListenersContainer()
    val featuresContainer: FeaturesContainer = FeaturesContainer(this)
    val voiceCommandsContainer = VoiceCommandsContainer()
    val voiceListeners = VoiceListenersContainer()

    init {
        playerManager.configuration.outputFormat = StandardAudioDataFormats.DISCORD_PCM_S16_BE
        playerManager.configuration.resamplingQuality = AudioConfiguration.ResamplingQuality.HIGH
        playerManager.setUseSeekGhosting(true)
        playerManager.frameBufferDuration = 20000
    }
}